const express = require("express");
//const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const mysql = require('mysql2');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'zucc_playground'
});

const app = express();

app.use(express.static("/var/www/html/zucc_playground_clone/build"));

const corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Serve React Build

app.get("/testApi", (req, res) => {
 res.json({ message: "Success!" });
});

// signup: piggyback on firebase-auth, save user data and firebase_uid to local database
app.post("/signup", (req, res) => {
    connection.execute(
        'INSERT INTO zucc_playground.user (username, firebase_uid, fullname) VALUES (?, ?, ?);',
        [req.body.username, req.body.uid, req.body.fullName],
        function (err) {
            if (err) {
                res.status(400).send('An error has occurred: ' + err);
            }
        });
});

// get user data
app.post("/allUser", (req, res) => {
    connection.execute(
        'SELECT * FROM zucc_playground.user where firebase_uid = ?;',
        [req.body.uid],
        function (err, result) {
            if (err) {
                res.status(400).send('An error has occurred: ' + err);
            } else if (result.length === 0) {
                res.status(400).send('Your account is not authenticated!');
            } else {
                connection.execute(
                    'SELECT * FROM zucc_playground.user;',
                    function (err, result) {
                        if (err) {
                            res.status(400).send('An error has occurred: ' + err);
                        } else res.json(result);
                    }
                )
            }
        });
});

// login: piggyback on firebase-auth, delete local user if user not found in Firebase

app.post("/login", (req, res) => {
    connection.execute(
        'DELETE FROM zucc_playground.user WHERE `username` = ?',
        [req.body.username],
        function(err) {
            if (err) {
                res.status(400).send('An error has occurred: ' + err);
            }
        }
    );
});


// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
