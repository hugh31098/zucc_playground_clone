import React, {Component} from "react";
import {auth, db} from "../services/firebase";

export default class ChatComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: auth().currentUser,
            chats: [],
            chat_session: null,
            content: '',
            readError: null,
            writeError: null,
            loadingChats: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.myRef = React.createRef();
    }

    async componentDidMount() {
        if (this.props.friend_id) {
            await db.ref('user/' + this.state.user.uid + '/friends/' + this.props.friend_id).once('value', snap => {
                if (snap.val()) {
                    let chat_session = snap.val();
                    this.setState({chat_session});
                } else {
                    let chat_session = db.ref("chat_session").push({uid: [this.state.user.uid, this.props.friend_id]}).key;
                    this.setState({chat_session});
                    db.ref('user/' + this.state.user.uid + '/friends').update({
                        [this.props.friend_id]: chat_session
                    });
                    db.ref('user/' + this.props.friend_id + '/friends').update({
                        [this.state.user.uid]: chat_session
                    });
                }

                db.ref('user/' + this.state.user.uid + '/notifications').child(this.props.friend_id).remove()
                    .catch(function(error) {
                        console.log("Notification invalidate failed: " + error.message)
                    });

                this.setState({ readError: null, loadingChats: true });
                this.getChatMessage();
                this.setState({ loadingChats: false });
            })
        }
    }

    getChatMessage() {
        const chatArea = this.myRef.current;
        let chats = [];
        db.ref("chat_session/" + this.state.chat_session + "/message").on("child_added", snapshot => {
            db.ref("chats/" + snapshot.key).on("value", dataSnapshot => {
                chats.push(dataSnapshot.val());
                chats.sort(function (a, b) { return a.timestamp - b.timestamp })
                this.setState({ chats });
                chatArea.scrollBy(0, chatArea.scrollHeight);
            });
        });
    }

    handleChange(event) {
        db.ref('user/' + this.state.user.uid + '/notifications').child(this.props.friend_id).remove()
            .catch(function(error) {
                console.log("Notification invalidate failed: " + error.message)
            });
        this.setState({
            content: event.target.value
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        this.setState({ writeError: null });
        const chatArea = this.myRef.current;
        try {
            let key = db.ref("chats").push({
                content: this.state.content,
                timestamp: Date.now(),
                uid: this.state.user.uid
            }).key;
            await db.ref("chat_session/" + this.state.chat_session + "/message").update({
                [key]: Date.now()
            }, (error) => {
                if (error === null) {
                    db.ref("user/" + this.props.friend_id + "/notifications").update({[this.state.user.uid] : true})
                }
            });
            this.setState({ content: '' });
            chatArea.scrollBy(0, chatArea.scrollHeight);
        } catch (error) {
            this.setState({ writeError: error.message });
        }
    }

    formatTime(timestamp) {
        const d = new Date(timestamp);
        return `${d.getDate()}/${(d.getMonth() + 1)}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
    }

    render() {
        return (
            <div>
                <div className="chat-area" ref={this.myRef}>
                    {/* loading indicator */}
                    {this.state.loadingChats ? <div className="spinner-border text-success" role="status">
                        <span className="sr-only">Loading...</span>
                    </div> : ""}
                    {/* chat area */}
                    {this.state.chats.map(chat => {
                        return <p key={chat.timestamp} className={"chat-bubble " + (this.state.user.uid === chat.uid ? "current-user" : "")}>
                            {chat.content}
                            <br />
                            <span className="chat-time float-right">{this.formatTime(chat.timestamp)}</span>
                        </p>
                    })}
                </div>
                <form onSubmit={this.handleSubmit} className="mx-3">
                    <textarea className="form-control" name="content" onChange={this.handleChange} value={this.state.content}/>
                    {this.state.error ? <p className="text-danger">{this.state.error}</p> : null}
                    <button type="submit" className="btn btn-submit px-5 mt-4">Send</button>
                </form>
                <div className="py-5 mx-3">
                    Login in as: <strong className="text-info">{this.state.user.email}</strong>
                </div>
            </div>
        );
    }
}
