import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from "./Dashboard/Title";
import { auth, db } from "../services/firebase";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

export default class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: auth().currentUser,
            friends: [],
            users: [],
            notification: false
        };
        this._isMounted = false;
        this.count = 0;
    }

    componentDidMount() {
        this._isMounted = true;

        db.ref('user/' + this.state.user.uid + '/notifications').on("value", () => {
            if (this.count !== 0) {
                let notificationState = !this.state.notification;
                this.setState({notification: notificationState})
            } else {
                this.count++;
            }
        })

        fetch('/allUser', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({uid: this.state.user.uid})
        }).then(response => {
            if (response.status === 400) {
                return response.text();
            } else {
                return response.json();
            }
        }).then(data => {
            db.ref('user').once('value').then(snapshot => {
                let users = snapshot.val();
                let friends = [];
                db.ref('user/' + this.state.user.uid + '/notifications').once('value').then(snap => {
                    let newMessage = snap.val();
                    for (let user in users) {
                        if (users.hasOwnProperty(user)) {
                            users[user].fullName = data[data.findIndex(x => x.firebase_uid === user)].fullname;
                            if (user === this.state.user.uid && users[user].friends) {
                                Object.keys(users[user].friends).forEach(key => {
                                    let notification = false;
                                    if (newMessage) {
                                        notification = (newMessage[key] === true);
                                    }
                                    friends[key] = {
                                        chat_session: users[user].friends[key],
                                        email: users[key].email,
                                        fullName: data[data.findIndex(x => x.firebase_uid === key)].fullname,
                                        notification: notification
                                    }
                                    delete users[key];
                                });
                            }
                        }
                    }
                    delete users[this.state.user.uid];
                    this.setState({users, friends});
                })
            })
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const handleClose = (event, reason) => {
            if (reason === 'clickaway') {
                return;
            }
            this.setState({notification: false})
        };

        return (
            <React.Fragment>
                <Title>Your Friends</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Email</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.keys(this.state.friends).map(key => (
                            <TableRow key={key}>
                                <TableCell style={{fontWeight: this.state.friends[key].notification ? 900 : 100}}>
                                    {this.state.friends[key].fullName}
                                </TableCell>
                                <TableCell style={{fontWeight: this.state.friends[key].notification ? 900 : 100}}>
                                    {this.state.friends[key].email}
                                </TableCell>
                                <TableCell style={{fontWeight: this.state.friends[key].notification ? 900 : 100}}>
                                    <Link to={{
                                        pathname: '/chat',
                                        friend_id: key
                                    }}>Chat</Link>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <br/>
                <Title>People You May Know</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Email</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.keys(this.state.users).map(key => (
                            <TableRow key={key}>
                                <TableCell>{this.state.users[key].fullName}</TableCell>
                                <TableCell>{this.state.users[key].email}</TableCell>
                                <TableCell>
                                    <Link to={{
                                        pathname: '/chat',
                                        friend_id: key
                                    }}>Chat</Link>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <Snackbar open={this.state.notification} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="info">
                        New messages has arrived! Reload page for more information.
                    </Alert>
                </Snackbar>
            </React.Fragment>
        );
    }
}