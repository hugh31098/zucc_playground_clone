import React, {Component} from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PostAddIcon from '@material-ui/icons/PostAdd';
import ViewListIcon from '@material-ui/icons/ViewList';
import FaceIcon from '@material-ui/icons/Face';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Link } from 'react-router-dom';
import { auth } from "../../services/firebase";

export default class SecondaryListItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginStatus: auth().currentUser,
        };
    }

    render() {
        return (
            <div>
                <ListSubheader inset>Chats</ListSubheader>
                <ListItem button component={Link} to='/friends'>
                    <ListItemIcon>
                        <FaceIcon />
                    </ListItemIcon>
                    <ListItemText primary="Friends List" />
                </ListItem>
                {this.state.loginStatus ?
                    <ListItem button onClick={() => auth().signOut()}>
                        <ListItemIcon>
                            <ExitToAppIcon />
                        </ListItemIcon>
                        <ListItemText primary="Sign Out" />
                    </ListItem>
                    :
                    <ListItem button component={Link} to='/login'>
                        <ListItemIcon>
                            <ExitToAppIcon />
                        </ListItemIcon>
                        <ListItemText primary="Login" />
                    </ListItem>
                }
            </div>
        )
    }
}
export const mainListItems = (
    <div>
        <ListSubheader inset>Blog Posting</ListSubheader>
        <ListItem button  component={Link} to='/'>
            <ListItemIcon>
                <PostAddIcon />
            </ListItemIcon>
            <ListItemText primary="My Posts" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <ViewListIcon />
            </ListItemIcon>
            <ListItemText primary="Latest Posts" />
        </ListItem>
    </div>
);
