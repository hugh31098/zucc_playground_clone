import React, {Component} from "react";
import {auth, db} from "../../services/firebase";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import IconButton from "@material-ui/core/IconButton";
import { Link } from 'react-router-dom';

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: auth().currentUser,
            counter: 0
        };
    }

    async componentDidMount() {
        if (this.state.user && this.state.user.uid) {
            await db.ref('user/' + this.state.user.uid + '/notifications').on('value', snap => {
                this.setState({counter: snap.numChildren()});
            })
        }
    }

    render() {
        return (
            <Link to="/friends">
                <IconButton color="inherit">
                    {this.state.counter !== 0 ?
                        <Badge badgeContent={this.state.counter} color="secondary">
                            <NotificationsIcon style={{ color: "white" }} />
                        </Badge>
                        :
                        <NotificationsIcon style={{ color: "white" }} />
                    }
                </IconButton>
            </Link>
        );
    }
}