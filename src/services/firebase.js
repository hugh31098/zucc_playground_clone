import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
const config = {
  apiKey: "AIzaSyD-n68h49B02mpgRlTaNr5P6cHD2eluIqc",
  authDomain: "web-project-48ece.firebaseapp.com",
  databaseURL: "https://web-project-48ece.firebaseio.com",
  projectId: "web-project-48ece",
  storageBucket: "web-project-48ece.appspot.com",
  messagingSenderId: "746011943832",
  appId: "1:746011943832:web:d9d76371a250d3b83581ed",
  measurementId: "G-8BRN4ZPHHF"

};

firebase.initializeApp(config);

export const auth = firebase.auth;
export const db = firebase.database();
