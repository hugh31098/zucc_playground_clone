import { auth } from "../services/firebase";
import { db } from "../services/firebase";

export async function signup(email, password, fullName) {
    await auth().createUserWithEmailAndPassword(email, password)
        .then((user) => {
            let {uid, email} = user.user;
            fetch('/signup', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({username: email, uid: uid, fullName: fullName})
            }).then(response => {
                if (response.status === 400) {
                    return response.text();
                }
            }).then(data => {
                if (data) alert(data);
            });
            return db.ref('user/' + uid).set({email: email});
            }, (reason => {
                alert("Signup failed: " + reason);
            })
        );
}

export function signin(email, password) {
  return auth().signInWithEmailAndPassword(email, password)
      .catch((error) => {
          alert("Wrong login credentials! Please try again.")
          if (error.code === 'auth/user-not-found') {
              fetch('/login', {
                  method: "POST",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({username: email})
              }).then(response => {
                  if (response.status === 400) {
                      return response.text();
                  }
              }).then(data => {
                  if (data) console.log(data);
              });
          }
      });
}

export function logout() {
  return auth().signOut();
}
